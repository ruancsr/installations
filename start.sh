# INSTALLATION COMMANDS
# (2) chmod +x start.sh
# (1) Enter your EMAIL and NAME
# (3) ./start.sh

echo "----------------------- START INSTALL --------------------------"

VERSION_NODE=14.17.3

VERSION_NVM=0.38.0

VERSION_AWS_CLI=2.2.2

VERSION_DOCKER_COMPOSE=1.29.2

EMAIL=
NAME=

echo "---------------------- CURL INSTALL ----------------------------"
sudo apt update
sudo apt install curl


echo "---------------------- NODE INSTALL ----------------------------"
sudo apt install nodejs
node -v

echo "----------------------- NPM INSTALL ----------------------------"
sudo apt install npm
npm -v

echo "---------------------- GIT INSTALL ----------------------------"
sudo apt install git
git config --global user.email "${EMAIL}"
git config --global user.name "${NAME}"
git --version

echo "---------------------- VIM INSTALL ----------------------------"
sudo apt install vim
vim --version

echo "--------------------- DOCKER INSTALL --------------------------"

sudo apt update
sudo apt install docker.io
docker --version

echo "----------------- DOCKER COMPOSE INSTALL ----------------------"
sudo curl -L "https://github.com/docker/compose/releases/download/${VERSION_DOCKER_COMPOSE}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

echo "--------------------- AWS-CLI INSTALL --------------------------"
sudo curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-${VERSION_AWS_CLI}.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
rm awscliv2.zip
rm -rf aws
aws --version

echo "-------------------- SERVELESS INSTALL -------------------------"
sudo npm install -g serverless
sls -v

sudo groupadd docker
sudo usermod -aG docker ${USER}
sudo chmod 666 /var/run/docker.sock
sudo systemctl restart docker

sudo apt install python3-pip

echo "------------------ INSTALLATION COMPLETED ----------------------"